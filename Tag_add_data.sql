DECLARE @RowCount INT
DECLARE @RowString VARCHAR(255)
DECLARE @Random INT
DECLARE @Upper INT
DECLARE @Lower INT
DECLARE @InsertTagId INT
DECLARE @InsertName VARCHAR(255)
DECLARE @InsertFlags INT

SET @Lower = 0
SET @Upper = 5000
SET @RowCount = 0

WHILE @RowCount < 170
BEGIN
	SET @RowString = CAST(@RowCount AS VARCHAR(255))
	SELECT @Random = ROUND(((@Upper - @Lower -1) * RAND() + @Lower), 0)
	SET @InsertTagId = @RowCount
	SET @InsertName = 'Expense1'
	SET @InsertFlags = 0
	
	INSERT INTO Tag
		(TagId
		,Name
		,Flags)
	VALUES
		(REPLICATE('0', 10 - DATALENGTH(@RowString)) + @RowString
		,'Expense1'
		,0)

	SET @RowCount = @RowCount + 1
	SET IDENTITY_INSERT Tag ON
END