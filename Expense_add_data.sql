DECLARE @RowCount INT
DECLARE @RowString VARCHAR(25)
DECLARE @Random INT
DECLARE @Upper INT
DECLARE @Lower INT
DECLARE @InsertExpenseId INT
DECLARE @InsertMemberId DECIMAL
DECLARE @InsertDate DATETIME
DECLARE @InsertAmount DECIMAL(19,5)
DECLARE @InsertTagId INT
DECLARE @InsertFlags INT

SET @Lower = 0
SET @Upper = 5000
SET @RowCount = 0

WHILE @RowCount < 170
BEGIN
	SET @RowString = CAST(@RowCount AS VARCHAR(10))
	SELECT @Random = ROUND(((@Upper - @Lower -1) * RAND() + @Lower), 0)
	SET @InsertExpenseId = @RowCount
	SET @InsertMemberId = @RowCount
	SET @InsertDate = DATEADD(dd, @Random, GETDATE())
	SET @InsertAmount = CAST(@InsertAmount AS decimal(19,5))
	SET @InsertTagId = @RowCount
	SET @InsertFlags = 0
	
	INSERT INTO Expense
		(ExpenseId
		,MemberId
		,Date
		,Amount
		,TagId
		,Flags)
	VALUES
		(REPLICATE('0', 10 - DATALENGTH(@RowString)) + @RowString
		,@RowCount
		,DATEADD(dd, @Random, GETDATE())
		,CAST(@InsertAmount AS decimal(19,5))
		,@RowCount
		,0)

	SET @RowCount = @RowCount + 1
	SET IDENTITY_INSERT Expense ON
END