DECLARE @RowCount INT
DECLARE @RowString VARCHAR(255)
DECLARE @Random INT
DECLARE @Upper INT
DECLARE @Lower INT
DECLARE @InsertMemberId INT
DECLARE @InsertLogin VARCHAR(255)
DECLARE @InsertName VARCHAR(255)
DECLARE @InsertPassword VARCHAR(255)
DECLARE @InsertFlags INT

SET @Lower = 0
SET @Upper = 5000
SET @RowCount = 0

WHILE @RowCount < 170
BEGIN
	SET @RowString = CAST(@RowCount AS VARCHAR(255))
	SELECT @Random = ROUND(((@Upper - @Lower -1) * RAND() + @Lower), 0)
	SET @InsertMemberId = @RowCount
	SET @InsertLogin = 'LoginName1'
	SET @InsertName = 'Expense1'
	SET @InsertPassword = CONVERT(VARCHAR(255), CRYPT_GEN_RANDOM(10))
	SET @InsertFlags = 0
	
	INSERT INTO Member
		(MemberId
		,Login
		,Name
		,Password
		,Flags)
	VALUES
		(REPLICATE('0', 10 - DATALENGTH(@RowString)) + @RowString
		,'LoginName1'
		,'Expense1'
		,CONVERT(VARCHAR(255), CRYPT_GEN_RANDOM(10))
		,0)

	SET @RowCount = @RowCount + 1
	SET IDENTITY_INSERT Member ON
END